<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\JobType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Job controller.
 *
 * @Route("job")
 */
class JobController extends Controller
{
    /**
     * Lists all job entities.
     *
     * @Route("/", name="job_index")
     * @Method("GET")
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('AppBundle:Job')->findAll();

        return $this->render('job/index.html.twig', [
            'jobs' => $jobs
        ]);
    }

    /**
     * Lists all job entities.
     *
     * @Route("/jobs", name="job_list")
     * @Method("GET")
     * @throws \LogicException
     */
    public function listJobAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('AppBundle:Job')->findAll();

        if (\count($jobs) === 0) {
            return new JsonResponse([
                'message' => 'Empty content'
            ], Response::HTTP_NO_CONTENT);
        }

        return $this->render('job/jobs.html.twig', [
            'jobs' => $jobs
        ]);
    }

    /**
     * Creates a new job entity.
     *
     * @Route("/create-job", name="job_new")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $data   = $request->getContent();
        $em = $this->getDoctrine()->getManager();
        $job_repo = $em->getRepository(Job::class);
        $job = $job_repo->findOneBy(['name' => $data]);

        if (\count($job) >= 1 && \is_object($job)) {
            return new JsonResponse([
                'message' => 'Job already exist'
            ], Response::HTTP_BAD_REQUEST);
        }

        $j = new Job();
        $j->setName($data);
        $em->persist($j);
        $em->flush();

        return new JsonResponse([
            'message' => 'Job created'
        ], Response::HTTP_CREATED);
    }
}
